'''
    File name: HANDBOOK
    Author: minhnc
    Date created(MM/DD/YYYY): 12/4/2018
    Last modified(MM/DD/YYYY HH:MM): 12/4/2018 7:36 AM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time
import numpy as np

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # The GPU id to use, usually either "0" or "1"

import torch

#==============================================================================
# Constant Definitions
#==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('--np', type=int, default=4096, help='number of input points(size of input point cloud)')
parser.add_argument('--ptn', type=str, default='./tmp/seg_model_94_0.944126.pth', help='patch of pre-trained model')
parser.add_argument('--idx', type=int, default=0, help='model index')

#==============================================================================
# Function Definitions
#==============================================================================
from data_loader import PartDataset

from model import PointNetPointWise
from torch.autograd import Variable

from points_visualization import visualize

#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is XXXXXX Program')

    opt = parser.parse_args()
    num_points = opt.np
    pretrained_model = opt.ptn
    idx = opt.idx
    root = 'E:/PROJECTS/NTUT/PointNet/pointnet1_pytorch/DATA/Shapenet/shapenetcore_partanno_segmentation_benchmark_v0'
    # root = './data/shapenetcore_partanno_segmentation_benchmark_v0'

    # data_trn = PartDataset(root=root, num_points=num_points, categories=['tools'], training=True, balanced=True,
    #                        shuffle=True, seed=0, offset=0)
    data_tes = PartDataset(root=root, num_points=num_points, categories=['tools'], training=True, balanced=True,
                           shuffle=True, seed=0, offset=0)
    # loader_trn = torch.utils.data.DataLoader(dataset=data_trn, batch_size=1, shuffle=True,
    #                                          num_workers=0)
    # loader_tes = torch.utils.data.DataLoader(dataset=data_tes, batch_size=1, shuffle=True,
    #                                          num_workers=0)
    num_classes = 3

    classifier = PointNetPointWise(num_points=num_points, num_classes=num_classes)
    classifier.load_state_dict(torch.load(pretrained_model))
    classifier.eval()

    points, labels = data_tes[idx]

    points_ts = torch.from_numpy(points)
    labels_ts = torch.from_numpy(labels)
    visualize(x=points[:, 0], y=points[:, 1], z=points[:, 2], label=labels, point_radius=0.0008) # visualize ground truth

    points_ts = points_ts.transpose(1, 0).contiguous()
    points_ts = Variable(points_ts.view(1, points_ts.size()[0], points_ts.size()[1]))
    pred, _ = classifier(points_ts)
    pred_labels = pred.data.max(2)[1] # get predicted labels
    pred_labels = pred_labels[0].data.cpu().numpy() # convert tensor to numpy array
    visualize(x=points[:, 0], y=points[:, 1], z=points[:, 2], label=pred_labels[0], point_radius=0.0008) # visualize predicted results

    pipe_points = points[pred_labels==2]
    visualize(x=pipe_points[:, 0], y=pipe_points[:, 1], z=pipe_points[:, 2], label=np.ones(len(pipe_points)), point_radius=0.0008) # visualize segmented objects

    points = torch.from_numpy(points)
    labels = torch.from_numpy(labels)

    points_tensor = points.transpose(1, 0).contiguous()
    points_tensor = Variable(points_tensor.view(1, points_tensor.size()[0], points_tensor.size()[1]))
    pred = classifier(points_tensor)
    pred_choice = pred.data.max(2)[1]

    visualize(x=points[:, 0], y=points[:, 1], z=points[:, 2], label=pred_choice[0], point_radius=0.0008)


if __name__ == '__main__':
    main()
