'''
    File name: HANDBOOK
    Author: minhnc
    Date created(MM/DD/YYYY): 11/30/2018
    Last modified(MM/DD/YYYY HH:MM): 11/30/2018 4:01 PM
    Python Version: 3.6
    Other modules: [None]

    Copyright = Copyright (C) 2017 of NGUYEN CONG MINH
    Credits = [None] # people who reported bug fixes, made suggestions, etc. but did not actually write the code
    License = None
    Version = 0.9.0.1
    Maintainer = [None]
    Email = minhnc.edu.tw@gmail.com
    Status = Prototype # "Prototype", "Development", or "Production"
    Code Style: http://web.archive.org/web/20111010053227/http://jaynes.colorado.edu/PythonGuidelines.html#module_formatting
'''

#==============================================================================
# Imported Modules
#==============================================================================
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os.path
import sys
import time
import math
import numpy as np

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"  # The GPU id to use, usually either "0" or "1"

import torch

#==============================================================================
# Constant Definitions
#==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('--np', type=int, default=1500, help='number of input points(size of input point cloud)')
parser.add_argument('--bs', type=int, default=2, help='input batch size')
parser.add_argument('--ne', type=int, default=10, help='number of epochs')
parser.add_argument('--ptn', type=str, default='', help='patch of pre-trained model')
parser.add_argument('--om', type=str, default='./tmp', help='saving-path of model weighs after training')
parser.add_argument('--nw', type=int, default=0, help='number of data loading workers(=0 with Windows)')

#==============================================================================
# Function Definitions
#==============================================================================
from data_loader import PartDataset

from model import PointNetPointWise
from torch.autograd import Variable
import torch.nn.functional as F
import torch.optim as optim

# from points_visualization import visualize

#==============================================================================
# Main function
#==============================================================================
def main(argv=None):
    print('Hello! This is PointNet segmentation program')

    opt = parser.parse_args()

    num_points = opt.np
    batch_size = opt.bs
    num_epochs = opt.ne
    num_workers = opt.nw
    # root = 'E:/PROJECTS/NTUT/PointNet/pointnet1_pytorch/DATA/Shapenet/shapenetcore_partanno_segmentation_benchmark_v0'
    root = './data/shapenetcore_partanno_segmentation_benchmark_v0'
    weight_file = opt.om

    data_trn = PartDataset(root=root, num_points=num_points, categories=['tools'], training=True, balanced=True, shuffle=True, seed=0, offset=0)
    data_tes = PartDataset(root=root, num_points=num_points, categories=['tools'], training=False, balanced=True, shuffle=True, seed=0, offset=0)
    loader_trn = torch.utils.data.DataLoader(dataset=data_trn, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    loader_tes = torch.utils.data.DataLoader(dataset=data_tes, batch_size=batch_size, shuffle=True, num_workers=num_workers)
    num_classes = data_trn.num_seg_classes

    ## Test visualizing data
    # idx_choices = 35
    # points, true_label = data_trn.__getitem__(idx_choices)
    # visualize(x=points[:, 0], y=points[:, 1], z=points[:, 2], label=true_label, point_radius=0.0008)

    point_wise_classifier = PointNetPointWise(num_points=num_points, num_classes=num_classes)
    optimizer = optim.SGD(point_wise_classifier.parameters(), lr=0.001, momentum=0.9)
    point_wise_classifier.cuda()

    blue = lambda x: '\033[94m' + x + '\033[0m'
    num_batch = len(data_trn)/batch_size
    for epoch in range(num_epochs):
        for i, data in enumerate(loader_trn, 0):
            points, labels = data
            points, labels = Variable(points), Variable(labels)
            points = points.transpose(2, 1)
            points, labels = points.cuda(), labels.cuda()
            optimizer.zero_grad()
            pred = point_wise_classifier(points)
            pred = pred.view(-1, num_classes)
            labels = labels.view(-1, 1)[:, 0] - 1
            loss = F.nll_loss(input=pred, target=labels)
            loss.backward()
            optimizer.step()
            pred_choice = pred.data.max(1)[1]
            correct = pred_choice.eq(labels.data).cpu().sum()
            print('[%d: %d/%d] train loss: %f accuracy: %f' % (epoch, i, num_batch, loss.item(), correct.item() / float(list(labels.shape)[0])))

        accuracies = []
        losses = []
        for j, data in enumerate(loader_tes, 0):
            points, target = data
            points, target = Variable(points), Variable(target)
            points = points.transpose(2, 1)
            points, target = points.cuda(), target.cuda()
            pred = point_wise_classifier(points)
            pred = pred.view(-1, num_classes)
            target = target.view(-1, 1)[:, 0] - 1

            loss = F.nll_loss(pred, target)
            pred_choice = pred.data.max(1)[1]
            correct = pred_choice.eq(target.data).cpu().sum()

            accuracies.append(correct.item() / len(points) / num_points)
            losses.append(loss.item())

            # tp = points.data.cpu().numpy()
            # tl = target.data.cpu().numpy()
            # tt = pred_choice.data.cpu().numpy()
            #
            # visualize(x=tp[0, 0, :], y=points[0, 1, :], z=points[0, 2, :], label=tl[:1500], point_radius=0.008)
            # visualize(x=tp[0, 0, :], y=points[0, 1, :], z=points[0, 2, :], label=tt[:1500], point_radius=0.008)

        average_acc = (np.sum(accuracies) / len(accuracies))
        average_loss = (np.sum(losses) / len(losses))
        print('[%d:] %s loss: %f accuracy: %f' % (epoch, blue('test'), average_loss, average_acc))

        torch.save(point_wise_classifier.state_dict(), '%s/seg_model_%d_%05f.pth' % (weight_file, epoch, average_acc))

    print("Finish")


if __name__ == '__main__':
    main()
